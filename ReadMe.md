# README #

This repository contains the supplemental material for the editors and reviewers.

### The main files 

* `1dExample.R` main file to reproduce the plots on the 1-dimensional analytical test case.
* `Section4.R` skeleton file to reproduce the benchmark presented in Section 4
* `Section5.R` main file to reproduce the results on the reliability engineering test case.
* `Section5suppMaterials.R` skeleton file to reproduce the benchmark in supplementary materials, section 5.


### The main folders

* `data` contains the data for the test case in Section 5 and for the 1-d example.

* `bin` contains all R files needed for running the experiments.


### The auxiliary packages

In order to run the code in the main files the latest version of the [`KrigInv`]( https://CRAN.R-project.org/package=KrigInv) package (available on CRAN) and its dependencies need to be installed.

