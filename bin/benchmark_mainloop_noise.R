#######################################################
## Function to benchmark one strategy on one testbed ##
#######################################################

# FIXME: Add parameter to control the type of safety condition ("<" or ">")

bench.run.onestrategy.onetestbed <- function (
    get.newPoints,                 # Sampling strategy
    strategy.name,                 # Name of strategy (used for filename only, NULL => no save)    
    bench.name,                    # Name of test bed (ex: "Matern32iso02")
    input.dim,                     # Dimension of input space    
    threshold = 1,                 # Threshold for safety condition
    list.idx.doe = seq (1, 100),   # DoE numbers
    list.idx.run = seq (1, 1000),  # Run numbers
    list.idx.qtau = NULL,          # a data.frame with q and tau for the current ID
    alpha = 0.95,                  # Probability parameter for conservative estimates
    nb.iter = 30,                  # Nb iterations of the sequential strategy
    fine.grid.size = 5000          # Size of fine grid for conservative estimates
)
{
    # Standard domain: [0; 1] ^ input.dim
    bounds <- list (lower = rep (0, times = input.dim),
                    upper = rep (1, times = input.dim))
    
    # Create result directory (if needed)
    if (! is.null (strategy.name)) {
        result.dir <- get.bench.result.dir (bench.name, input.dim, strategy.name,idx.qtau = list.idx.qtau)
        dir.create (result.dir, recursive = TRUE)
    }
    
    ###########################################################################
    # A sub-function that does all the job for ONE run
    bench.run.onestrategy.onefunction <- function (idx.doe, idx.run,list.idx.qtau)
    {
        # Load initial data (and restore RNG if appropriate)
        initdata <- bench.load.initdata (bench.name, input.dim = input.dim,
                                         idx.doe = idx.doe, idx.run = idx.run,idx.qtau = list.idx.qtau)
        
        # Extract fields
        x.init <- initdata$x.init
        y.init <- initdata$y.init
        gp0.desc <- initdata$gp0.desc
        
        # GP model: same as generating GP, for now (with unknown mean)
        gp.desc <- gp0.desc
        
        # Initialize generating GP (conditioned on initial data)
        gp0 <- km (formula = ~1, design = x.init, response = y.init,
                   covtype = gp0.desc$cov.type, coef.trend = 0,
                   coef.var = gp0.desc$param.var, coef.cov = gp0.desc$param.range,noise.var = rep(gp0.desc$param.noise^2,nrow(x.init)))
        
        # Compute everything (model, predictions, CE...) on the initial design
        state <- state.init (x.init = x.init, y.init = y.init, alpha = alpha,
                             threshold = threshold, fine.grid.size = fine.grid.size,
                             covtype = gp.desc$cov.type, coef.var = gp.desc$param.var,
                             coef.cov = gp.desc$param.range, coef.noise = gp.desc$param.noise)
        
        # Save initial state
        history <- state.prepare.history (nb.iter)    
        history <- state.save (state = state, history = history, i = 1)
        history$critVals <- rep(NA,nb.iter)
        
        # Iterate !
        for (i.iter in seq (nb.iter))
        {
            # output the number of iteration
            cat("#############################")
            cat("## n.iter: ",i.iter," of ",nb.iter,"##")
            cat("#############################")
          
            # Get new points using sequential strategy
            state$auxHybrid=i.iter
            tmp <- get.newPoints (state, bounds)
            x.new <- tmp$newPoints
            
            if(list.idx.qtau$q<0){
              alt_q <- abs(list.idx.qtau$q+1)
              alt_x.new <- lhsDesign (n = alt_q, dimension = ncol(x.new))$design
              x.new  <- rbind(x.new, alt_x.new)
            }
            
            # Simulate responses
            y.new <- simulate (object = gp0, cond = TRUE, newdata = x.new,nugget.sim=1e-8)
            if(!is.null(gp.desc$param.noise) && gp.desc$param.noise>0)
              y.new <- y.new + matrix(rnorm(1,sd=gp.desc$param.noise),ncol=nrow(x.new))
            
            if(ncol(y.new)>1){
              y.new = t(y.new)
            }
            
            # Re-compute everything on the augmented design and then save history
            state <- state.update (state, x.new, y.new, cov.reestim = FALSE)
            history <- state.save (state = state, history = history, i = i.iter + 1)
            history$critVals[i.iter] <- tmp$aux$value
            
            
            # Update the generating model
            gp0 <- update (object = gp0, newX = x.new, newy = y.new,
                           cov.reestim = FALSE, trend.reestim = TRUE,nugget.reestim= FALSE,newnoise.var=gp0.desc$param.noise^2)       
        }
        
        state.reduced<-state
        state.reduced$pred = NULL
        state.reduced$fine.grid<-NULL
        
        
        res<-list(history=history,state=state.reduced)
        
        return (res)
    }
    
    ###########################################################################
    all.results <- list ();
    k <- 0;  k.max <- length (list.idx.doe) * length (list.idx.run)
    
    for (idx.doe in list.idx.doe) {
        for (idx.run in list.idx.run) {
            
            k <- k + 1;  cat (sprintf ("doe= %d, run =%d, k = %d/%d... ",idx.doe,idx.run, k, k.max))
            
            # check if the file already exists (skip iterations)
            fileToSave<-get.bench.result.filename (
              bench.name, input.dim, strategy.name, idx.doe, idx.run,list.idx.qtau)
            if(file.exists(fileToSave)){
              cat ("File already present, restoring rng...")
              RNGkind (kind = "Mersenne-Twister", normal.kind = "Inversion") 
              load(fileToSave)
              if(is.null(result$rng.info)){
                cat("Cannot restore rng, running it again\n")
                cat (sprintf ("k = %d/%d... ", k, k.max))
              }else{
                .Random.seed <<- result$rng.info
                cat ("rng restored, skip to next k.\n")
                next
              }
            }
    
            time <- system.time (
                output.log <- capture.output (
                    res <- bench.run.onestrategy.onefunction (idx.doe, idx.run,list.idx.qtau)))
            
            # Save final RNG state    
            rng.info <- .Random.seed
            
            result <- list (idx.doe = idx.doe, idx.run = idx.run,
                            history = res$history, time = time, output.log = output.log,
                            rng.info=rng.info)
            lastState=res$state
            all.results[[k]] <- result
            
            if (! is.null (strategy.name))
                save (result,lastState, file = get.bench.result.filename (
                    bench.name, input.dim, strategy.name, idx.doe, idx.run,list.idx.qtau))
            
            cat (sprintf ("done  (%.2f minutes)\n", time[[1]] / 60))
        }
    }
    return (all.results)
}


# Name of "result" directory
get.bench.result.dir <- function (bench.name, input.dim, strategy.name,idx.qtau) {
    paste (getwd (), "res/bench", "results", bench.name, 
           sprintf ("dim%03d", input.dim), sprintf("q%02d", idx.qtau$q), strategy.name, sep = "/") }

# Filename for a given "result" file
get.bench.result.filename <- function (bench.name, input.dim, strategy.name,
                                       idx.doe, idx.run,idx.qtau) {
    paste (get.bench.result.dir (bench.name, input.dim, strategy.name,idx.qtau),
           sprintf ("doe%04d_tau%02d_run%04d.RData", idx.doe, round(idx.qtau$tau*100), idx.run), sep = "/") }
